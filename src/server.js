'use strict';

const express = require('express');
const webpackBuilder = require('./webpack/index.js');
const { resolve } = require("path");
const fs = require("fs");
// Constants 
const PORT = 8080;
const HOST = '0.0.0.0'; 

// App 
const app = express();
app.get('/', (req, res) => {    
    res.send({
        'hello': 'there',
    });
});

app.get('/webpack', function(req, res) {
    const whatIsThisNow = webpackBuilder(); 
    const bundle1 = fs.readFileSync(resolve(__dirname, 'dist', 'index1.bundle.js')); 
    const bundle2 = fs.readFileSync(resolve(__dirname, 'dist', 'index2.bundle.js')); 
    const debuggingInfo = {
        'bundle1': bundle1,
        'bundle2': bundle2,
        'one of these days': 'Ill learn how to do this async',
    };
    console.log('debuggingInfo', debuggingInfo);
    res.send({
        "hello": "there", 
        "whatIsThisNow": whatIsThisNow
    });
});

app.listen(PORT, HOST); 
console.log(`Running on http://${HOST}:${PORT}`);