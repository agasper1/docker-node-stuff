const webpack = require("webpack");
const fs = require("fs");

const webpackBuilder = () => {
    const configuration = {
        entry: {
            index1: './index1.js',
            index2: './index2.js', 
        }, 
        output: {
            filename: '[name].bundle.js',
            chunkFilename: '[name].bundle.js',
        }
    };
    // webpack(configurationObject)
    const compiler = webpack(configuration);
    
    compiler.run((error, stats) => {
        if (error || stats.hasErrors()) {
            // handle errors here 
        }
        // Done Processing 
        console.log(stats.toJson());
    });
}



module.exports = webpackBuilder;