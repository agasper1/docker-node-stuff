[ECS Ref. Arch] (https://github.com/aws-samples/ecs-refarch-cloudformation/blob/master/infrastructure/security-groups.yaml) 
Statement
    Effect 
    Action: action
    Resource: arn 



ServiceRole:
    Type: AWS::IAM::Role
    Properties: !Sub ecs-service-${AWS:::StackName}
    Path: / 
    AssumeRolePolicyDocument: | 
    { 
        "Statement": [{
            "Effect": "Allow",
            "Principal": { "Service": [ "ecs.amazonaws.com" ]}, 
            "Action": [ "sts:AssumeRole" ]
        }]
    }
    Policies:
        - PolicyName: !Sub ecs-service-${AWS-StackName}