FROM node:8
# Create app directory
WORKDIR /usr/src/app
# From node:8 => 'free' node + npm
COPY ["src/package*.json", "/usr/src/app/"]
# COPY package*.json ./
# Install dependencies; production flag? 
RUN [ "npm", "install" ]
# Bundle app source 
# Assumes the execution context is the same as the 
# source code path (context => probably wrong word)
COPY ["src/", "/usr/src/app"]
# Expose port 8080
EXPOSE 8080
# Oh yeah, run it 
CMD [ "npm", "start" ]