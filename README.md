# Module Bundling 
- So I was like :thinking_face: what if you could bundle your stuff independent of any process? Just kind of like standalone 
- Which meant I would need somewhere to do it.
- And a _stuff_ bundler 


Eventually this should in a lambda somewhere 


1. Build the image 
`docker build -t name/node-web-app-webpack .` 
2. Run the image 
`docker run -p 49160:8080 -d name/node-web-app-webpack` 


Since I haven't quite figure out how to get the docker image to watch the host file system for changes...

1. Get the name of the image running 
`docker ps` 
2. Kill the image 
`docker kill priceless_khayyam`
3. Rebuild the image 
`docker build -t name/node-web-app-webpack .` 
4. Run the image 
`docker run -p 49160:8080 -d name/node-web-app-webpack` 


You can _kind of_ test this by going 

1. Navigate to `localhost:49160/webpack`
2. `docker logs {name_of_the_running_docker_image}`
3. You can see me outputting the result of the compilation. 


That being said, I think I'm trying to pull the file with that read sync before the webpack configuration has a chance to finish.




docker exec inspiring_taussig cat /usr/src/app/dist/index1.bundle.js >> output_of_cat_index1.bundle.js

# The Webpack Configuration 
- Pretty barebones 
    - Just looks for those two files
    - No fancy test, but if I wanted, this could be extended to probably parse with babel


Enters, then outputs to a file. 