api-messaging/bitbucket-pipelines.yaml
```
image:
  name: >-
    263307080745.dkr.ecr.us-west-2.amazonaws.com/kaena-docker-builder-image:latest
  aws:
    access-key: $AWS_ACCESS_KEY_ID
    secret-key: $AWS_SECRET_ACCESS_KEY
```



repo -triggers->pipeline 
pipeline looks to dockerfile for build 
save build to ECR 
then trigger the ansible tower 
ansible tower deploys the new ecr image to the existing container/lambda 

https://nodejs.org/en/docs/guides/nodejs-docker-webapp/

`docker build -t <username>/<node-web-app-name> /path/to/dockerfile`
- Or you can pass a file 
`docker run -p 49160:8080 -d agasper/node-web-app-example`
`949ed9dfa2c00febdf0b1df25c1538eb3b65d5ec83c3541f10ca4f1652964730`


- Get the container ID 
  - `docker ps`
- Print app output 
  - docker logs <container id> 
- Example 


Should Probably learn how to put the new image without killing the image each time 

Something about a local version of the registry 